// SPD_INSA.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
using namespace std;

int i;
int czySkonczone;
int ileWstawionoDoTop;

int maximum(int a, int b)
{
	if (a > b) return a;
	else return b;
}

void wypisz(int lZadan, int lMaszyn, int **tablica)
{
	for (int i = 1; i <= lMaszyn*lZadan; i++)
	{
		for (int j = 1; j <= 10; j++)
		{
			cout << tablica[i][j] << "\t";
		}
		cout << endl;
	}
	cout << endl;
	cout << endl;
}

// Funkcja liczaca ilosc poprzednikow (technologiczny + kolejnosciowy)
void policzIloscPoprzednikow(int lZadan, int lMaszyn, int **tablica)
{
	for (int i = 1; i <= lMaszyn*lZadan; i++)
	{
		tablica[i][8] = 0;
		if (tablica[i][5] != 0) tablica[i][8]++;
		if (tablica[i][7] != 0) tablica[i][8]++;
	}
}

// Funkcja liczaca kolejnosc topologiczna
void policzKolejnoscTopologiczna(int lZadan, int lMaszyn, int **tablica, int *kolTopologiczna)
{
	policzIloscPoprzednikow(lZadan, lMaszyn, tablica);
	int gdzieWstawic = 1;

	while (gdzieWstawic <= lZadan*lMaszyn)
	{
		ileWstawionoDoTop = 0;
		for (int i = 1; i <= lMaszyn*lZadan; i++)
		{

			if (tablica[i][8] == 0 || tablica[i][8] == -1)
			{
				kolTopologiczna[gdzieWstawic] = tablica[i][1];
				//	cout << "Wstawilem " << tablica[i][1] << " na miejsce " << gdzieWstawic << endl;
				gdzieWstawic++;
				tablica[i][8] = -20;
				ileWstawionoDoTop++;
			}
		}

		// Jezeli mial jakiegos poprzednika, ktorego wrzucilismy do topologicznej, to zmniejsz ilosc poprzednikow
		for (int e = gdzieWstawic - 1; e >= gdzieWstawic - ileWstawionoDoTop; e--)
		{
			for (int w = 1; w <= lMaszyn*lZadan; w++)
			{
				if (tablica[w][5] == kolTopologiczna[e]) tablica[w][8]--;
				if (tablica[w][7] == kolTopologiczna[e]) tablica[w][8]--;
			}
		}
	}
}

void uzupelnijKolumneNr9(int lZadan, int lMaszyn, int **tablica, int *kolTopologiczna)
{
	int jakiPoprzednikTech;

	//Teraz mamy leciec po kolejnosci topologicznej i uzupelniac kolumne 9
	for (int i = 1; i <= lZadan*lMaszyn; i++)
	{
		tablica[kolTopologiczna[i]][9] = 0;
		// sprawdzamy czy jest poprzednik technologiczny, jezeli jest to dodajemy jego czas
		if (tablica[kolTopologiczna[i]][5] != 0)
		{
			jakiPoprzednikTech = tablica[kolTopologiczna[i]][5];
			tablica[kolTopologiczna[i]][9] += tablica[jakiPoprzednikTech][9];
		}


		// jezeli ma poprzednika kolejnosciowego to bierzemy max R z tych poprzednichow
		if (tablica[kolTopologiczna[i]][7] != 0)
		{
			tablica[kolTopologiczna[i]][9] = 0;
			jakiPoprzednikTech = tablica[kolTopologiczna[i]][5];
			int jakiPoprzednikKolej = tablica[kolTopologiczna[i]][7];
			int coBierzemy = maximum(tablica[jakiPoprzednikTech][9], tablica[jakiPoprzednikKolej][9]);
			tablica[kolTopologiczna[i]][9] += coBierzemy;
		}
		tablica[kolTopologiczna[i]][9] += tablica[kolTopologiczna[i]][3];
		//cout << "Dla zadania " << kolTopologiczna[i] << " R wyszlo nam " << tablica[kolTopologiczna[i]][9] << endl;
	}
}

void uzupelnijKolumneNr10(int lZadan, int lMaszyn, int **tablica, int *kolTopologiczna)
{
	int jakiNastepnikTech;
	//Teraz mamy leciec od konca topologicznej i uzupelniac kolumne 10
	for (int i = lZadan*lMaszyn; i >= 1; i--)
	{
		tablica[kolTopologiczna[i]][10] = 0;
		// sprawdzamy czy jest nastepnik technolog, jezeli jest to dodajemy jego czas
		if (tablica[kolTopologiczna[i]][4] != 0)
		{
			jakiNastepnikTech = tablica[kolTopologiczna[i]][4];
			tablica[kolTopologiczna[i]][10] += tablica[jakiNastepnikTech][10];
		}

		// jezeli ma nastepnika kolejnosciowego to bierzemy max R z tych poprzednichow
		if (tablica[kolTopologiczna[i]][6] != 0)
		{
			tablica[kolTopologiczna[i]][10] = 0;
			jakiNastepnikTech = tablica[kolTopologiczna[i]][4];
			int jakiNastepnikKolej = tablica[kolTopologiczna[i]][6];
			int coBierzemy = maximum(tablica[jakiNastepnikTech][10], tablica[jakiNastepnikKolej][10]);
			tablica[kolTopologiczna[i]][10] += coBierzemy;
		}
		tablica[kolTopologiczna[i]][10] += tablica[kolTopologiczna[i]][3];
	}
}

void sortujPoCzasach(int lZadan, int lMaszyn, int **tablica, int **posortowaneProcesy)
{
	int **pomocnicza = new int *[(lMaszyn*lZadan) + 1];

	for (int i = 0; i <= lMaszyn*lZadan; i++)
		pomocnicza[i] = new int[2];

	for (int i = 1; i <= lZadan*lMaszyn; i++)
	{
		pomocnicza[i][0] = i;
		pomocnicza[i][1] = tablica[i][3];
	}

	for (int j = 1; j <= lZadan*lMaszyn; j++)
	{
		int maxTime = 0;
		int indeks;
		for (int i = 1; i <= lZadan*lMaszyn; i++)
		{

			if (pomocnicza[i][1] > maxTime)
			{
				maxTime = pomocnicza[i][1];
				indeks = pomocnicza[i][0];
			}
		}
		posortowaneProcesy[j][0] = indeks;
		posortowaneProcesy[j][1] = maxTime;
		maxTime = 0;
		pomocnicza[indeks][1] = 0;
	}
}


int main()
{
	string nazwaPliku;
	ifstream plikWej;
	ofstream plikWyj;
	int lMaszyn;
	int lZadan;

	plikWej.open("bench_js.txt");
	plikWyj.open("bench_js_output.txt");
	// Bo jest 80 instancji
	for (int h=1;h<=80;h++)
	{
	plikWej >> nazwaPliku;
	plikWej >> lZadan;
	plikWej >> lMaszyn;

	int *kolTopologiczna = new int[(lMaszyn*lZadan) + 1];

	//Tworzenie tablicy
	int ** tablica = new int *[(lMaszyn*lZadan) + 1];

	for (int i = 0; i <= lMaszyn*lZadan; i++)
		tablica[i] = new int[10];

	//Wypelnianie tablicy
	for (int i = 0; i <= lMaszyn*lZadan; i++)
	{
		tablica[i][1] = i;
		tablica[i][4] = i + 1;
		tablica[i][5] = i - 1;
		tablica[i][6] = 0;
		tablica[i][7] = 0;
		tablica[i][8] = 0;
		tablica[i][9] = 0;
		tablica[i][10] = 0;
	}

	//Wczytywanie danych
	for (int i = 1; i <= lMaszyn*lZadan; i++)
	{
		plikWej >> tablica[i][2];
		plikWej >> tablica[i][3];

		//zerowanie niektorych nastepnikow technologicznych
		if (tablica[i][1] % lMaszyn == 0) tablica[i][4] = 0;

		//zerowanie niektorych poprzednikow technologicznych
		if (tablica[i][1] % lMaszyn == 1) tablica[i][5] = 0;
	}

	int **wynikowa = new int *[(lMaszyn)+1];

	for (int i = 0; i <= lMaszyn; i++)
		wynikowa[i] = new int[lZadan + 2];

	for (int i = 1; i <= lMaszyn; i++)
		for (int j = 1; j <= lZadan + 1; j++) wynikowa[i][j] = 0;

	int **posortowaneProcesy = new int *[(lMaszyn*lZadan) + 1];

	for (int i = 0; i <= lMaszyn*lZadan; i++)
		posortowaneProcesy[i] = new int[2];

	i = 0;
	czySkonczone = 0;
	ileWstawionoDoTop = 0;
	clock_t begin = clock();
	while (!czySkonczone)
	{
		policzKolejnoscTopologiczna(lZadan, lMaszyn, tablica, kolTopologiczna);



		uzupelnijKolumneNr9(lZadan, lMaszyn, tablica, kolTopologiczna);

		uzupelnijKolumneNr10(lZadan, lMaszyn, tablica, kolTopologiczna);

		sortujPoCzasach(lZadan, lMaszyn, tablica, posortowaneProcesy);

		//robimy tablice w ktorej wrzucamy zadanka na maszyny, maszyna 1 to 1 wiersz, 2 to 2 itd

		i++;
		int naJakaMaszyne;
		int numerZadania;
		int naKtoreMiejsce = 0;
		int wstawiono = 0;
		int *najlepsze = new int[lZadan + 2];
		for (int x = 0; x <= lZadan + 1; x++) najlepsze[x] = 0;

		int *uszeregowanie = new int[lZadan + 2];
		for (int x = 0; x <= lZadan + 1; x++) uszeregowanie[x] = 0;
		numerZadania = posortowaneProcesy[i][0];
		naJakaMaszyne = tablica[numerZadania][2];

		// Dopoki niczego nie wstawimy
		while (wstawiono == 0)
		{
			naKtoreMiejsce++;

			if (wynikowa[naJakaMaszyne][naKtoreMiejsce] == 0)
			{
				wynikowa[naJakaMaszyne][naKtoreMiejsce] = numerZadania;
				wstawiono = 1;
			}


			if (wstawiono == 1 && naKtoreMiejsce != 1)//jezeli tak, to musimy sprawdzic rozne cmaxy
			{
				int najmniejszyCMax = 999999;
				for (int y = 1; y <= naKtoreMiejsce; y++) uszeregowanie[y] = wynikowa[naJakaMaszyne][y];
				int iloscPermutacji = 0;
				for (int j = naKtoreMiejsce; j >= 1; j--)
				{
					int poprzednikTech = tablica[numerZadania][5];
					int poprzednikKole = uszeregowanie[naKtoreMiejsce - iloscPermutacji - 1];
					int RpoprzednikaTech = tablica[poprzednikTech][9];
					int RpoprzednikaKole = tablica[poprzednikKole][9];
					int nastepnikTech = tablica[numerZadania][4];
					int nastepnikKole = uszeregowanie[naKtoreMiejsce + 1 - iloscPermutacji];
					int RnastepnikaTech = tablica[nastepnikTech][10];
					int RnastepnikaKole = tablica[nastepnikKole][10];
					int cmax = maximum(RpoprzednikaKole, RpoprzednikaTech) + tablica[numerZadania][3] + maximum(RnastepnikaKole, RnastepnikaTech);

					//cout << "Cmax wynosi " << cmax << endl;
					// Jezeli cmax jest mniejszy niz nasz najmniejszy to podmieniamy
					if (cmax <= najmniejszyCMax)
					{
						najmniejszyCMax = cmax;
						// i zapamietujemy permutacje w tablicy
						for (int y = 0; y <= naKtoreMiejsce; y++)
						{
							najlepsze[y] = uszeregowanie[y];
						}
					}
					// Czas na ulubione permutacje
					iloscPermutacji++;
					int pom1 = uszeregowanie[j - 1];
					uszeregowanie[j - 1] = uszeregowanie[j];
					uszeregowanie[j] = pom1;
				}
				//Wpisujemy do wynikowej najlepsza kolejnosc
				for (int i = 1; i <= lZadan; i++) wynikowa[naJakaMaszyne][i] = najlepsze[i];

				// Uzupelniamy nastepnikow kolejnosciowych korzystajac z wektora najlepsze
				for (int i = 1; i <= naKtoreMiejsce; i++)
				{
					int poprzednik = najlepsze[i - 1];
					int nastepnik = najlepsze[i + 1];
					tablica[najlepsze[i]][7] = poprzednik;
					tablica[najlepsze[i]][6] = nastepnik;
				}
			}
		}
		int ileNastepnikow = 0;
		int ilePoprzednikow = 0;
		czySkonczone = 0;

		// Tutaj chodzi o to, ze konczymy liczyc, jezeli ilosc poprzednikow = ilosc nastepnikow = liczba maszyn

		for (int i = 1; i <= lMaszyn*lZadan; i++)
		{
			if (tablica[i][6] == 0) ileNastepnikow++;
			if (tablica[i][7] == 0) ilePoprzednikow++;

		}
		if ((ileNastepnikow == lMaszyn) && (ilePoprzednikow == lMaszyn)) czySkonczone = 1;

	}

	// Liczymy to jeszcze raz, aby uzupelnic ostatni raz kolumne R i Q
	policzKolejnoscTopologiczna(lZadan, lMaszyn, tablica, kolTopologiczna);
	uzupelnijKolumneNr9(lZadan, lMaszyn, tablica, kolTopologiczna);
	uzupelnijKolumneNr10(lZadan, lMaszyn, tablica, kolTopologiczna);

	// Liczymy najwieksze R w kolumnie
	int jakiWynik = 0;
	for (int i = 1; i <= lZadan*lMaszyn; i++)
		if (tablica[i][9] > jakiWynik) jakiWynik = tablica[i][9];



	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;



	/////////////////////////////////////////////
	// Jezeli chcemy macierz ta glowna
	//wypisz(lZadan, lMaszyn, tablica);

	//////////////////////////////////////////
	// Jezeli chcemy macierze uszeregowan
	/*
	for (int i = 1; i <= lMaszyn; i++)
	{
		for (int j = 1; j <= lZadan; j++) cout<< wynikowa[i][j] << "\t";
		cout  << endl;
	}cout << endl;*/


	//////////////////////////////////////
	//Jezeli chcemy same Cmaxy
	cout << jakiWynik << endl;
	
}

	return 0;
}
